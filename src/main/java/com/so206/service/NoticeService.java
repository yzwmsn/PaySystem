package com.so206.service;

import com.so206.po.SystemNotice;
import com.so206.utils.PageBean;

import java.util.List;

public interface NoticeService {

    int saveNotice(SystemNotice notice);

    SystemNotice findById(Integer id);

    int updateNotice(SystemNotice notice);

    void deleteNotice(Integer id);

    PageBean<SystemNotice> findNoticeByPage(Integer page, Integer rows, Integer status);

    List<SystemNotice> findDisplayNotices();


}
