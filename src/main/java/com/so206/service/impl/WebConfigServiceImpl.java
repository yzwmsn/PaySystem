package com.so206.service.impl;

import com.so206.mapper.SystemWebMapper;
import com.so206.po.SystemWebWithBLOBs;
import com.so206.service.WebConfigService;
import com.so206.utils.BeanCheck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class WebConfigServiceImpl implements WebConfigService {

    @Autowired
    private SystemWebMapper webMapper;

    @Override
    public SystemWebWithBLOBs find_by_id(Integer id) {
        return webMapper.selectByPrimaryKey(id);
    }

    @Override
    public void update_web_config(SystemWebWithBLOBs webconfig) {
        SystemWebWithBLOBs web = webMapper.selectByPrimaryKey(webconfig.getId());
        if (web != null) {
            BeanCheck.copyPropertiesIgnoreNull(webconfig, web);
            webMapper.updateByPrimaryKeyWithBLOBs(web);
        }
    }


}
